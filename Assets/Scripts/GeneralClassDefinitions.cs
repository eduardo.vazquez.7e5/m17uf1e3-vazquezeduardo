using Newtonsoft.Json;

public enum PlayerClass
{
    Warrior,
    Rogue,
    Wizard
};

public class PlayerInfo
{
    public string Name { get; set; }
    public PlayerClass playerClass { get; set; }
    public float Height { get; set; }
    public float Speed { get; set; }
    public float DistanceTravelled { get; set; }

    public PlayerInfo()
    {
        Name = "Edu";
        playerClass = PlayerClass.Warrior;
        Height = 1.5f;
        Speed = 1f;
        DistanceTravelled = 2f;
    }
    
    public string Serialize()
    {
        string result = "";
        result += Name.Replace(';',':');
        result += ";" + playerClass.ToString();
        result += ";" + JsonConvert.SerializeObject(Height);
        result += ";" + JsonConvert.SerializeObject(Speed);
        result += ";" + JsonConvert.SerializeObject(DistanceTravelled);
        return result;
    }

    public static PlayerInfo Deserialize(string input)
    {
        string[] inputArray = input.Split(';');
        PlayerInfo result = new PlayerInfo();

        result.Name = inputArray[0];
        switch (inputArray[1])
        {
            case "Warrior":
            default:
                result.playerClass = PlayerClass.Warrior;
                break;
            case "Rogue":
                result.playerClass = PlayerClass.Rogue;
                break;
            case "Wizard":
                result.playerClass = PlayerClass.Wizard;
                break;
        }
        result.Height = JsonConvert.DeserializeObject<float>(inputArray[2]);
        result.Speed = JsonConvert.DeserializeObject<float>(inputArray[3]);
        result.DistanceTravelled = JsonConvert.DeserializeObject<float>(inputArray[4]);

        return result;
    }

    public static PlayerClass IntToPlayerClass(int n)
    {
        switch (n)
        {
            default:
                return PlayerClass.Warrior;
            case 1:
                return PlayerClass.Rogue;
            case 2:
                return PlayerClass.Wizard;
        }
    }

    public static int PlayerClassToInt(PlayerClass playerClass)
    {
        switch (playerClass)
        {
            case PlayerClass.Warrior:
            default:
                return 0;
            case PlayerClass.Rogue:
                return 1;
            case PlayerClass.Wizard:
                return 2;
        }
    }
    
    public static string PlayerClassToString(PlayerClass playerClass)
    {
        switch (playerClass)
        {
            case PlayerClass.Warrior:
            default:
                return "Guerrera";
            case PlayerClass.Rogue:
                return "Lladre";
            case PlayerClass.Wizard:
                return "Bruixota";
        }
    }
}