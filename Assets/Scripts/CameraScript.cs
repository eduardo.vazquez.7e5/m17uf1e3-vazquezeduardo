using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] 
    private ButtonScript cameraButton;
    private bool _isFollowing;
    
    void Start()
    {
        _isFollowing = true;
    }
    
    void Update()
    {
        if (cameraButton.JustClicked())
            _isFollowing = !_isFollowing;
        if (_isFollowing)
        {
            float x = GameObject.Find("Player").GetComponent<Transform>().position.x;
            GetComponent<Transform>().position = new Vector3(x - 1.5f, 0, -10);
        }
    }
}
