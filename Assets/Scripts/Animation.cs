using UnityEngine;

public class Animation
{
    private readonly Sprite[][] _spriteFrames;
    private int _currentSprite;
    private readonly float _fps;
    private readonly SpriteRenderer _spriteRenderer;
    private int _spriteIndex;
    private float _currentTime;
    private float _transitionTime;
    public bool Frozen { get; set; }

    public Animation(Sprite[][] spriteFrames, int currentSprite, float fps, SpriteRenderer spriteRenderer)
    {
        Frozen = false;
        _currentSprite = currentSprite;
        _spriteFrames = spriteFrames;
        _fps = fps;
        _spriteRenderer = spriteRenderer;
    }
    
    public void Start()
    {
        _currentTime = 0f;
        _transitionTime = 1 / _fps;
        _spriteIndex = 0;
        _spriteRenderer.sprite = _spriteFrames[_currentSprite][_spriteIndex];
    }

    public void Update()
    {
        if(!Frozen)
            _currentTime += Time.deltaTime;
        if (_currentTime >= _transitionTime)
        {
            _currentTime -= _transitionTime;
            _spriteIndex = ++_spriteIndex % _spriteFrames[_currentSprite].Length;
            _spriteRenderer.sprite = _spriteFrames[_currentSprite][_spriteIndex];
        }
    }

    public void SetSprite(int spriteIndex)
    {
        _currentSprite = spriteIndex;
        _spriteRenderer.sprite = _spriteFrames[_currentSprite][_spriteIndex];
    }
}
