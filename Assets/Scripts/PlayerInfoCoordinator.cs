using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoCoordinator : AnotherUIManager
{
    private PlayerInfo _playerInfo;
    [SerializeField]
    private DataPlayer dataPlayer;

    private bool _isLoading = true;
    
    private float[] _previousDeltas;
    private int _numberOfFrames;
    private float _sumOfDeltas;
    private int _deltaIndex;
    void Start()
    {
        string infoFromPreviousScreen = PlayerPrefs.GetString("PlayerInfo");
        if (string.IsNullOrEmpty(infoFromPreviousScreen))
            _playerInfo = new PlayerInfo();
        else
            _playerInfo = PlayerInfo.Deserialize(infoFromPreviousScreen);

        UpdateDisplayers();
        _isLoading = false;

        _previousDeltas = Array.Empty<float>();
        _numberOfFrames = 200;
        _sumOfDeltas = 0;
        _deltaIndex = 0;
    }

    private void Update()
    {
        UpdateDeltas();
        UpdateNameDisplayer();
    }

    private void UpdateDisplayers()
    {
        GameObject.Find("PlayerNameInput").GetComponent<InputField>().text = _playerInfo.Name;
        Dropdown dropdown = GameObject.Find("PlayerClassInput").GetComponent<Dropdown>();
        dropdown.value = PlayerInfo.PlayerClassToInt(_playerInfo.playerClass);
        GameObject.Find("PlayerHeightInput").GetComponent<Slider>().value = _playerInfo.Height;
        base.UpdateHeightLabel();
        GameObject.Find("PlayerSpeedInput").GetComponent<Slider>().value = _playerInfo.Speed;
        base.UpdateSpeedLabel();
        GameObject.Find("PlayerDistanceTravelledInput").GetComponent<Slider>().value = _playerInfo.DistanceTravelled;
        base.UpdateDistanceTravelledLabel();
    }
    
    public void UpdateName()
    {
        if (!_isLoading)
        {
            _playerInfo.Name = GameObject.Find("PlayerNameText").GetComponent<Text>().text;
            dataPlayer.Name = _playerInfo.Name;
        }
    }
    
    public override void UpdateHeightLabel()
    {
        base.UpdateHeightLabel();
        if (!_isLoading)
        {
            _playerInfo.Height = GameObject.Find("PlayerHeightInput").GetComponent<Slider>().value;
            dataPlayer.Height = _playerInfo.Height;
        }
    }
    
    public override void UpdateSpeedLabel()
    {
        base.UpdateSpeedLabel();
        if (!_isLoading)
        {
            _playerInfo.Speed = GameObject.Find("PlayerSpeedInput").GetComponent<Slider>().value;
            dataPlayer.Speed = _playerInfo.Speed;
        }
    }
    
    public override void UpdateDistanceTravelledLabel()
    {
        base.UpdateDistanceTravelledLabel();
        if (!_isLoading)
        {
            _playerInfo.DistanceTravelled =
                GameObject.Find("PlayerDistanceTravelledInput").GetComponent<Slider>().value;
            dataPlayer.DistanceTravelled = _playerInfo.DistanceTravelled;
        }
    }

    public override void ShowDropDownValue()
    {
        base.ShowDropDownValue();
        if (!_isLoading)
        {
            var value = GameObject.Find("PlayerClassInput").GetComponent<Dropdown>().value;
            PlayerClass playerClass = PlayerInfo.IntToPlayerClass(value);
            _playerInfo.playerClass = playerClass;
            dataPlayer.SetPlayerClass(playerClass);
        }
    }
    
    public void UpdatePowerDurationLabel()
    {
        var value = GameObject.Find("PlayerPowerDurationInput").GetComponent<Slider>().value;
        GameObject.Find("PlayerPowerDurationDisplayer").GetComponent<Text>().text = "Power Time: " +  value.ToString("0.0");
        if (!_isLoading)
            dataPlayer.totalGiantTime = value;
    }

    public void UpdateNameDisplayer()
    {
        GameObject displayer = GameObject.Find("PlayerNamePanel");
        GameObject nameText = GameObject.Find("PlayerNameDisplayer");
        GameObject player = GameObject.Find("Player");
        Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();

        string tipus = PlayerInfo.PlayerClassToString(_playerInfo.playerClass);
        nameText.GetComponent<Text>().text = _playerInfo.Name + " " + tipus;
        
        Vector3 playerBot = player.GetComponent<SpriteRenderer>().bounds.min;
        Vector3 playerTop = player.GetComponent<SpriteRenderer>().bounds.max;
        var playerScreenBot = camera.WorldToScreenPoint(playerBot);
        var playerScreenTop = camera.WorldToScreenPoint(playerTop);
        displayer.transform.position = new Vector3((playerScreenBot.x+playerScreenTop.x)/2, playerScreenTop.y,0) + new Vector3(0, 30, 0);
    }

    private void UpdateDeltas()
    {
        if (_previousDeltas.Length < _numberOfFrames)
            Array.Resize(ref _previousDeltas, _previousDeltas.Length+1);
        else
            _sumOfDeltas -= _previousDeltas[_deltaIndex];
        float delta = Time.deltaTime;
        _sumOfDeltas += delta;
        _previousDeltas[_deltaIndex] = delta;
        _deltaIndex = ++_deltaIndex%_numberOfFrames;
        
        GameObject.Find("FpsDisplayer").GetComponent<Text>().text = "FPS: " + (_previousDeltas.Length/_sumOfDeltas).ToString("0.00");
    }
}
