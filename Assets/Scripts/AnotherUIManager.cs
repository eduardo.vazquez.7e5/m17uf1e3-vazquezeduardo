using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AnotherUIManager : MonoBehaviour
{
    [SerializeField] 
    private Sprite[] ClassSprites;
    
    public void QuieroJugar()
    {
        PlayerInfo playerInfo = ReadPlayerInfo();
        PlayerPrefs.SetString("PlayerInfo", playerInfo.Serialize());
        SceneManager.LoadScene(1);
    }

    protected PlayerInfo ReadPlayerInfo()
    {
        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.Name = GameObject.Find("PlayerNameInput").GetComponent<InputField>().text;
        Dropdown dropdown = GameObject.Find("PlayerClassInput").GetComponent<Dropdown>();
        string classSelected = dropdown.options[dropdown.value].text;
        switch (classSelected)
        {
            case "Guerrera":
            default:
                playerInfo.playerClass = PlayerClass.Warrior;
                break;
            case "Lladre":
                playerInfo.playerClass = PlayerClass.Rogue;
                break;
            case "Bruixota":
                playerInfo.playerClass = PlayerClass.Wizard;
                break;
        }
        playerInfo.Height = GameObject.Find("PlayerHeightInput").GetComponent<Slider>().value;
        playerInfo.Speed = GameObject.Find("PlayerSpeedInput").GetComponent<Slider>().value;
        playerInfo.DistanceTravelled = GameObject.Find("PlayerDistanceTravelledInput").GetComponent<Slider>().value;

        return playerInfo;
    }

    public virtual void UpdateHeightLabel()
    {
        var value = GameObject.Find("PlayerHeightInput").GetComponent<Slider>().value;
        GameObject.Find("PlayerHeightDisplayer").GetComponent<Text>().text = "Height: " +  value.ToString("0.00");
    }
    
    public virtual void UpdateSpeedLabel()
    {
        var value = GameObject.Find("PlayerSpeedInput").GetComponent<Slider>().value;
        GameObject.Find("PlayerSpeedDisplayer").GetComponent<Text>().text = "Speed: " +  value.ToString("0.00");
    }
    
    public virtual void UpdateDistanceTravelledLabel()
    {
        var value = GameObject.Find("PlayerDistanceTravelledInput").GetComponent<Slider>().value;
        GameObject.Find("PlayerDistanceTravelledDisplayer").GetComponent<Text>().text = "Distance: " +  value.ToString("0.00");
    }

    public virtual void ShowDropDownValue()
    {
        var value = GameObject.Find("PlayerClassInput").GetComponent<Dropdown>().value;
        GameObject.Find("PlayerClassDisplayer").GetComponent<Image>().sprite = ClassSprites[value];
    }
}
