using System;
using UnityEngine;
using UnityEngine.UI;

public class DataPlayer : MonoBehaviour
{
    enum WalkingStates{ 
        Caminar,
        Esperar,
        Lliure
    };

    enum PowerStates
    {
        NormalSize,
        Growing,
        MaximumSize,
        Reducing,
        Cooldown
    }

    public string Name;
    public PlayerClass playerClass;
    public float Height;
    public float Speed;
    public float DistanceTravelled;
    private Animation _animation;
    private FiniteStateMachine<WalkingStates> _walkingManager;
    private FiniteStateMachine<PowerStates> _powerManager;
    [SerializeField] 
    private Sprite[] Sprites;
    [SerializeField] 
    private float fps = 12;
    [SerializeField]
    private float tempsDEspera = 1.5f;
    [SerializeField]
    private float totalGrowingTime = 1;
    [SerializeField] 
    public float totalGiantTime = 1;
    [SerializeField] 
    private float cooldownGiantTime = 2;

    public float weight = 1;
    private float realWeight;
    [SerializeField] 
    private ButtonScript powerButton;
    [SerializeField] 
    private ButtonScript manualButton;


    private Vector3 velocitat;
    
    void Start()
    {
        Sprite[][] spriteFrames = new Sprite[3][];
        for (int i = 0; i < 3; i++)
        {
            Sprite[] spriteArray = {Sprites[3 * i], Sprites[3 * i + 1], Sprites[3 * i + 2]};
            spriteFrames[i] = spriteArray;
        }

        string infoFromPreviousScreen = PlayerPrefs.GetString("PlayerInfo");
        PlayerInfo playerInfo;
        if (string.IsNullOrEmpty(infoFromPreviousScreen))
            playerInfo = new PlayerInfo();
        else
            playerInfo = PlayerInfo.Deserialize(infoFromPreviousScreen);
        SaveInfo(playerInfo);
        
        velocitat = new Vector3(Speed, 0, 0);
        realWeight = weight;
        
        int currentSprite = GetCurrentSprite(playerInfo.playerClass);
        _animation = new Animation(spriteFrames, currentSprite, fps, GetComponent<SpriteRenderer>());
        _animation.Start();

        _walkingManager = new FiniteStateMachine<WalkingStates>(WalkingStates.Caminar);
        SetUpWalkingManager();
        _walkingManager.Start();
        
        _powerManager = new FiniteStateMachine<PowerStates>(PowerStates.NormalSize);
        SetUpPowerManager();
        _powerManager.Start();
    }
    
    void Update()
    {
        _animation.Update();
        _walkingManager.Update();
        _powerManager.Update();
    }

    private void SetUpWalkingManager()
    {
        float tempsEsperat = 0;
        float distanciaRecorreguda = 0;
        WalkingStates previousState = WalkingStates.Caminar;
      
        _walkingManager.SetNode(
            WalkingStates.Caminar,
            () => StartCaminarAutonom(out distanciaRecorreguda),
            () => CaminarAutonom(ref distanciaRecorreguda),
            _ => manualButton.JustClicked(),
            _ => WalkingStates.Lliure,
            _ => distanciaRecorreguda >= DistanceTravelled,
            _ => WalkingStates.Esperar
        );

        _walkingManager.SetNode(
            WalkingStates.Esperar,
            () =>
            {
                tempsEsperat = 0;
                _animation.Frozen = true;
            },
            () => tempsEsperat += Time.deltaTime,
            () =>
            {
                _animation.Frozen = false;
                velocitat = - velocitat;
            },
            _ => manualButton.JustClicked(),
            _ => WalkingStates.Lliure,
            _ => tempsEsperat >= tempsDEspera,
            _ => WalkingStates.Caminar
        );

        _walkingManager.SetNode(
            WalkingStates.Lliure,
            () => previousState = _walkingManager.GetCurrentState(),
            CaminarAmbInput,
            () => _animation.Frozen = false,
            _ => manualButton.JustClicked(),
            _ => previousState
        );
    }

    private void SetUpPowerManager()
    {
        float accumulatedTime = 0;
        Transform transform = GetComponent<Transform>();
        float alçadaObjectiu = Height;

        _powerManager.SetNode(
            PowerStates.NormalSize,
            () => transform.localScale = new Vector3(1, 1, 1),
            () => {},
            _ => powerButton.JustClicked(),
            _ => PowerStates.Growing
        );

        _powerManager.SetNode(
            PowerStates.Growing,
            () =>
            {
                accumulatedTime = 0;
                alçadaObjectiu = Height;
            },
            () => ModifySize(ref accumulatedTime, Time.deltaTime, alçadaObjectiu, transform),
            _ => accumulatedTime >= totalGrowingTime,
            _ => PowerStates.MaximumSize
        );
        
        _powerManager.SetNode(
            PowerStates.MaximumSize,
            () =>
            {
                transform.localScale = new Vector3(alçadaObjectiu, alçadaObjectiu, 1);
                accumulatedTime = 0;
            },
            () => accumulatedTime += Time.deltaTime,
            _ => accumulatedTime >= totalGiantTime,
            _ => PowerStates.Reducing
        );
        
        _powerManager.SetNode(
            PowerStates.Reducing,
            () => accumulatedTime = totalGrowingTime,
            () => ModifySize(ref accumulatedTime, -Time.deltaTime, alçadaObjectiu, transform),
            _ => accumulatedTime <= 0,
            _ => PowerStates.Cooldown
        );

        _powerManager.SetNode(
            PowerStates.Cooldown,
            () => accumulatedTime = 0,
            () => accumulatedTime += Time.deltaTime,
            _ => accumulatedTime >= cooldownGiantTime,
            _ => PowerStates.NormalSize);
    }

    private void CaminarAmbInput()
    {
        Transform rb = GetComponent<Transform>();
        float xInput = Input.GetAxis("Horizontal");
        Vector3 inputVector = new Vector3(xInput, 0, 0);
        velocitat = Speed * inputVector / realWeight;
        if(velocitat.magnitude > 0)
            GetComponent<SpriteRenderer>().flipX = velocitat.x > 0;
        rb.position += velocitat * Time.deltaTime;
        _animation.Frozen = velocitat.magnitude == 0;
    }

    private void StartCaminarAutonom(out float distanciaRecorreguda)
    {
        distanciaRecorreguda = 0;
        GetComponent<SpriteRenderer>().flipX = velocitat.x > 0;
    }
    
    private void CaminarAutonom(ref float distanciaRecorreguda)
    {
        velocitat = Speed * velocitat.normalized / realWeight;
        Transform transform = GetComponent<Transform>();
        transform.position += velocitat * Time.deltaTime;
        distanciaRecorreguda += velocitat.magnitude * Time.deltaTime;
    }

    private void ModifySize(ref float accumulatedTime, float deltaTime, float alçadaObjectiu, Transform transform)
    {
        accumulatedTime += deltaTime;
        float scale = 1 + (alçadaObjectiu-1) * Between(accumulatedTime / totalGrowingTime,0f, 1f);
        realWeight = weight * scale;
        transform.localScale = new Vector3(scale, scale, 1);
    }

    private static float Between(float value, float min, float max)
    {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    }
    
    private static int GetCurrentSprite(PlayerClass playerClass){
        switch (playerClass)
        {
            case PlayerClass.Warrior: return 0;
            case PlayerClass.Rogue: return 1;
            case PlayerClass.Wizard: return 2;
        }
        return 0;
    }

    public void SetPlayerClass(PlayerClass playerClass)
    {
        switch (playerClass)
        {
            case PlayerClass.Warrior:
            default:
                playerClass = PlayerClass.Warrior;
                _animation.SetSprite(0);
                break;
            case PlayerClass.Rogue:
                playerClass = PlayerClass.Rogue;
                _animation.SetSprite(1);
                break;
            case PlayerClass.Wizard:
                playerClass = PlayerClass.Wizard;
                _animation.SetSprite(2);
                break;
        }
    }

    private void SaveInfo(PlayerInfo playerInfo)
    {
        Name = playerInfo.Name;
        playerClass = playerInfo.playerClass;
        Height = playerInfo.Height;
        Speed = playerInfo.Speed;
        DistanceTravelled = playerInfo.DistanceTravelled;
    }
}
