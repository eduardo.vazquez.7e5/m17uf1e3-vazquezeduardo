using System;
using System.Collections.Generic;
using JetBrains.Annotations;

public class Edge<T>
{
    
    public readonly Predicate<object> Condition;
    
    public readonly Func<object, T> FinalState;

    public Edge(Predicate<object> condition, Func<object, T> finalState)
    {
        Condition = condition;
        FinalState = finalState;
    }
}

public class Node<T>
{
    public List<Edge<T>> Edges { get; set; }

    public readonly Action Start;

    public readonly Action Update;

    [CanBeNull] public readonly Action End;

    public Node(Action start, Action update)
    {
        Edges = new List<Edge<T>>();
        Start = start;
        Update = update;
        End = null;
    }

    public Node(Action start, Action update, Action end)
    {
        Edges = new List<Edge<T>>();
        Start = start;
        Update = update;
        End = end;
    }

    public void SetEdge( Predicate<object> condition, Func<object, T> finalState)
    {
        Edges.Add(new Edge<T>(condition, finalState));
    }
}

public class FiniteStateMachine<T> where T : Enum
{
    private T _currentState;
    private readonly Dictionary<T, Node<T>> _stateNode = new();

    public FiniteStateMachine(T initialState){
        _currentState = initialState;
    }

    public void Start()
    {
        _stateNode[_currentState].Start.Invoke();
    }
    
    public void Update()
    {
        _currentState = CheckConditions(_currentState);
        _stateNode[_currentState].Update.Invoke();
    }
    
    private T CheckConditions(T state)
    {
        Node<T> currentNode = _stateNode[_currentState];
        foreach (Edge<T> edge in currentNode.Edges)
            if (edge.Condition.Invoke(null))
            {
                if(currentNode.End != null)
                    currentNode.End.Invoke();
                T finalState = edge.FinalState.Invoke(null);
                _stateNode[finalState].Start.Invoke();
                return finalState;
            }
        return state;
    }

    public void SetEdge(T initialState, Predicate<object> condition, Func<object, T> finalState)
    {
        if (_stateNode.ContainsKey(initialState))
            _stateNode[initialState].SetEdge(condition, finalState);
    }
    
    public void RemoveAllEdges(T state)
    {
        if (_stateNode.ContainsKey(state))
            _stateNode[state].Edges = new List<Edge<T>>();
    }

    public T GetCurrentState()
    {
        return _currentState;
    }

    public void SetNode(T state, Node<T> node)
    {
        if (_stateNode.ContainsKey(state))
            _stateNode.Remove(state);
        _stateNode.Add(state, node);
    }
    
    public void SetNode(T state, Action start, Action update, Predicate<object> condition, Func<object, T> finalState)
    {
        Node<T> node = new Node<T>(start, update);
        node.SetEdge(condition, finalState);
        SetNode(state, node);
    }
    
    public void SetNode(T state, Action start, Action update, Action end, Predicate<object> condition, Func<object, T> finalState)
    {
        Node<T> node = new Node<T>(start, update, end);
        node.SetEdge(condition, finalState);
        SetNode(state, node);
    }
    
    public void SetNode(T state, Action start, Action update,  Predicate<object> condition0, Func<object, T> finalState0, Predicate<object> condition1, Func<object, T> finalState1)
    {
        Node<T> node = new Node<T>(start, update);
        node.SetEdge(condition0, finalState0);
        node.SetEdge(condition1, finalState1);
        SetNode(state, node);
    }

    public void SetNode(T state, Action start, Action update, Action end, Predicate<object> condition0, Func<object, T> finalState0, Predicate<object> condition1, Func<object, T> finalState1)
    {
        Node<T> node = new Node<T>(start, update, end);
        node.SetEdge(condition0, finalState0);
        node.SetEdge(condition1, finalState1);
        SetNode(state, node);
    }
}